#BUILD STAGE
FROM golang:alpine AS builder

#add label to be able to filter temporary images
LABEL stage=builder

#set working directory outside gopath
WORKDIR /mail_service
COPY . .
RUN apk add --no-cache git && \
    go get -d -v ./...

RUN go build -o ./mail_service

# --------------------------------------------------

#FINAL STAGE
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /mail_service

#copy the whole project folder from temporary image and set entrypoint
COPY --from=builder /mail_service/mail_service .
ENTRYPOINT ./mail_service
LABEL Name=mail_service Version=0.1
EXPOSE 5001


## build image and remove temporary images:
#docker build -t mail_service . && docker image prune --filter label=stage=builder -f