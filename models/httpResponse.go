package models

type intrfc interface{}

// HTTPResponse standar json response
type HTTPResponse struct {
	Status int16  `json:"status,omitempty"`
	Error  string `json:"error,omitempty"`
	Data   intrfc `json:"data,omitempty"`
}
