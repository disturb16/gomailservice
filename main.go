package main

import (
	"fmt"
	"gomailservice/routes"
	"gomailservice/utils"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()
	routes.GetMailRoutes(router)

	// start server
	port := utils.GetEnv("PORT", "5050")
	fmt.Println("server running on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
