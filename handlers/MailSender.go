package handlers

import (
	"crypto/tls"
	"gomailservice/settings"
	"gomailservice/utils"
	"net/smtp"
)

// SendMail ...
func SendMail(to []string, subject string, message string) error {

	mail := &settings.Mail{}
	mail.ToIds = to
	mail.Subject = subject
	mail.Body = message

	smtpServer := &settings.SmtpServer{Host: "smtp.gmail.com", Port: "465"}

	// log.Println(smtpServer.host)
	//build an auth
	sender := utils.GetEnv("MAIL_SENDER", "devtesting160@gmail.com")
	password := utils.GetEnv("MAIL_SENDER_PWD", "Abc123**")
	auth := smtp.PlainAuth("", sender, password, smtpServer.Host)

	// Gmail will reject connection if it's not secure
	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpServer.Host,
	}

	conn, err := tls.Dial("tcp", smtpServer.ServerName(), tlsconfig)
	if err != nil {
		return err
	}

	client, err := smtp.NewClient(conn, smtpServer.Host)
	if err != nil {
		return err
	}

	// step 1: Use Auth
	if err = client.Auth(auth); err != nil {
		return err
	}

	// step 2: add all from and to
	if err = client.Mail(mail.SenderId); err != nil {
		return err
	}
	for _, k := range mail.ToIds {
		if err = client.Rcpt(k); err != nil {
			return err
		}
	}

	// Data
	w, err := client.Data()
	if err != nil {
		return err
	}

	// send message
	messageBody := mail.BuildMessage()
	_, err = w.Write([]byte(messageBody))
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	client.Quit()

	return nil
}
