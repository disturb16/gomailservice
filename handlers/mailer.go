package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"gomailservice/utils"
	"log"
	"net/http"
	"strings"
)

type mailPayload struct {
	Recipients  []string `json:"recipients"`
	Subject     string   `json:"subject,omitempty"`
	MessageBody string   `json:"messageBody"`
}

// HandleMail sends mails using gmail account
func HandleMail(w http.ResponseWriter, req *http.Request) {

	requestID := req.Header.Get("Kong-Request-ID")
	apiKey := req.Header.Get("api-key")

	// decode payload
	payload := &mailPayload{}
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&payload)

	if err != nil {
		logError(apiKey, requestID, payload, err)
		utils.SendJSONError(w, err, nil)
		return
	}

	// check receivers paramter
	if len(payload.Recipients) == 0 {
		err = errors.New("email receivers is empty")
		logError(apiKey, requestID, payload, err)
		utils.SendJSONError(w, err, nil)
		return
	}

	// check messageBody paramter
	if len(payload.MessageBody) == 0 {
		err = errors.New("messageBody is empty")
		logError(apiKey, requestID, payload, err)
		utils.SendJSONError(w, err, nil)
		return
	}

	// send emails
	err = SendMail(payload.Recipients, payload.Subject, payload.MessageBody)

	if err != nil {
		logError(apiKey, requestID, payload, err)
		utils.SendJSONError(w, err, nil)
		return
	}

	// send response
	logSuccess(apiKey, requestID, payload)
	utils.SendJSONResponse(w, "Mail(s) sent successfully")
}

func logError(apiKey string, requestID string, payload *mailPayload, err error) {
	recipients := strings.Join(payload.Recipients[:], ",")
	logMessage := fmt.Sprintf("requestId='%s', apiKey='%s', mail_Sent='false', email_recipients='%s' error='%s'", requestID, apiKey, recipients, err.Error())
	log.Println(logMessage)
}

func logSuccess(apiKey string, requestID string, payload *mailPayload) {
	recipients := strings.Join(payload.Recipients[:], ",")
	logMessage := fmt.Sprintf("requestId='%s', apiKey='%s', mail_Sent='true', email_recipients='%s'", requestID, apiKey, recipients)
	log.Println(logMessage)
}
