package routes

import (
	"gomailservice/handlers"

	"github.com/gorilla/mux"
)

// GetMailRoutes returns initial subroutes
func GetMailRoutes(router *mux.Router) {
	apiMail := router.PathPrefix("/service/mail/api/v1").Subrouter()
	apiMail.HandleFunc("/send", handlers.HandleMail).Methods("POST")
}
