package loggers

import (
	"fmt"
	"log"
	"path/filepath"
	"runtime"
)

// LogError ...
func LogError(msg string, v ...interface{}) {

	_, file, line, _ := runtime.Caller(1)
	log.Printf("\033[31m Error on  %s: line:%d \033[39m", append([]interface{}{filepath.Base(file), line})...)
	fmt.Printf("\t"+msg+"\n", v...)
}
