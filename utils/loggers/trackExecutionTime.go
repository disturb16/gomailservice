package loggers

import (
	"fmt"
	"log"
	"time"
)

// TrackExecutionTime logs time elapsed for caller function
func TrackExecutionTime(caller string, t time.Time) {
	elapsed := time.Since(t)

	if caller == "" {
		fmt.Println("TrackExecutionTime Caller is nil...")
	}

	log.Println(fmt.Sprintf("Function=\"%s\" ms=\"%f\"", caller, float64(elapsed.Nanoseconds())/float64(time.Millisecond)))
}
