package utils

import (
	"encoding/json"
	"gomailservice/models"
	"net/http"
)

// SendJSONResponse sends standar json response
func SendJSONResponse(w http.ResponseWriter, data interface{}) {
	response := &models.HTTPResponse{}
	response.Status = http.StatusOK
	response.Data = data
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// SendJSONError sends json response with specified error
func SendJSONError(w http.ResponseWriter, err error, data interface{}) {
	response := &models.HTTPResponse{}
	response.Error = err.Error()
	response.Data = data
	response.Status = http.StatusInternalServerError
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
